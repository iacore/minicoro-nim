# The official example in readme: at https://github.com/edubart/minicoro

import std/unittest

import minicoro

type Foo = object
  bar: int
  baz: int

var transcript: seq[int] = @[]

proc coro_entry(co: Coro) {.cdecl.} =
  var foo: Foo
  co.pop(foo)
  check foo.bar == 64222
  check foo.baz == 2375
  transcript &= 1
  co.yield
  transcript &= 3

proc main =
  var foo: Foo
  foo.bar = 64222
  foo.baz = 2375

  var desc = initCoroDesc(coro_entry, 0)
  desc.user_data = nil

  let co = desc.create()
  defer: co.destroy
  
  check co.status == coSUSPENDED
  
  var s = [1'u8, 2, 3]
  co.pushBytes(s)
  check co.bytes_stored == s.len
  
  co.push(foo)
  
  transcript &= 0
  co.resume
  
  check co.status == coSUSPENDED
  transcript &= 2
  
  co.resume
  
  check co.status == coDEAD
  transcript &= 4
  
  s = [0, 0, 0]
  co.popBytes(s)
  check s == [1'u8, 2, 3]

  check transcript == [0, 1, 2, 3, 4]

test "Simple usage (Nim API integration)":
  main()
