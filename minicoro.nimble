# Package

version       = "0.1.3"
author        = "Locria Cyber"
description   = "Nim wrapper for minicoro - a tiny Lua-like asymmetric coroutine library"
license       = "Unlicense"
srcDir        = "src"


# Dependencies

# I'm not sure which version will do
requires "nim >= 1.6"
