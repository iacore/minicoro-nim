Coroutine in Nim, courtesy of https://github.com/edubart/minicoro

Warning: minicoro is not tested without `--mm:orc` or `--mm:arc`

## Usage

```nim
import minicoro

proc coro_entry(co: Coro) {.cdecl.} =
  var data: int
  co.pop(data)
  echo data # print 42

  co.yield


var desc = initCoroDesc(coro_entry, 0)
let co = desc.create()

co.push(42.int) # each corotine has its own storage stack

assert co.status == coSUSPENDED
co.resume
assert co.status == coSUSPENDED
co.resume
assert co.status == coDEAD

co.destroy()
```

For another example, see [this test file](tests/test1.nim)
For complete API, see [source code](src/minicoro.nim)
