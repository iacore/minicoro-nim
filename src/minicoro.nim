from std/os import parentDir, `/`


const useOrcArc = defined(gcArc) or defined(gcOrc)

when not useOrcArc:
  {.warning: "minicoro is not tested without --mm:orc or --mm:arc".}

const HEADER_PATH = currentSourcePath.parentDir() / "minicoro.h"

{.localPassC: "-DMINICORO_IMPL".}
{.localPassC: "-DMCO_NO_DEBUG".}

type
  CoroEntry* = proc (co: ptr mco_coro) {.cdecl.}
  
  mco_coro {.importc, header: HEADER_PATH.} = object
  Coro* = ptr mco_coro

  mco_desc = CoroDesc
  CoroDesc* {.importc: "mco_desc", header: HEADER_PATH.} = object
    co*: CoroEntry
    user_data*: pointer
    malloc_cb*: proc(size: uint, allocator_data: pointer) {.cdecl.}
    free_cb*: proc(p: pointer, allocator_data: pointer) {.cdecl.}
    allocator_data*: pointer
    storage_size*: uint
    coro_size*: uint
    stack_size*: uint
  
  mco_result = CoroResult
  CoroResult* {.pure, importc: "mco_result", header: HEADER_PATH.} = enum
    SUCCESS = 0,
    GENERIC_ERROR,
    INVALID_POINTER,
    INVALID_COROUTINE,
    NOT_SUSPENDED,
    NOT_RUNNING,
    MAKE_CONTEXT_ERROR,
    SWITCH_CONTEXT_ERROR,
    NOT_ENOUGH_SPACE,
    OUT_OF_MEMORY,
    INVALID_ARGUMENTS,
    INVALID_OPERATION,
    STACK_OVERFLOW,

  mco_state = CoroState
  CoroState* {.importc: "mco_state", header: HEADER_PATH.} = enum
    ## The coroutine has finished normally or was uninitialized before finishing.
    coDEAD = 0,
    ## The coroutine is active but not running (that is, it has resumed another coroutine).
    coNORMAL,
    ## The coroutine is active and running.
    coRUNNING,
    ## The coroutine is suspended (in a call to yield, or it has not started running yet).
    coSUSPENDED

{.push, importc, header: HEADER_PATH.}
# Coroutine functions.

## Initialize description of a coroutine. When stack size is 0 then coDEFAULT_STACK_SIZE is used.
proc mco_desc_init(co: CoroEntry, stack_size: uint): mco_desc
## Initialize the coroutine.
proc mco_init(co: ptr mco_coro, desc: ptr mco_desc): mco_result
## Uninitialize the coroutine, may fail if it's not dead or suspended.
proc mco_uninit(co: ptr mco_coro): mco_result
## Allocates and initializes a new coroutine.
proc mco_create(out_co: ptr ptr mco_coro, desc: ptr mco_desc): mco_result
## Uninitialize and deallocate the coroutine, may fail if it's not dead or suspended.
proc mco_destroy(co: ptr mco_coro): mco_result
## Starts or continues the execution of the coroutine.
proc mco_resume(co: ptr mco_coro): mco_result
## Suspends the execution of a coroutine.
proc mco_yield(co: ptr mco_coro): mco_result
## Returns the status of the coroutine.
proc mco_status(co: ptr mco_coro): mco_state
## Get coroutine user data supplied on coroutine creation.
proc mco_get_user_data(co: ptr mco_coro): pointer

# Storage interface functions, used to pass values between yield and resume.

## Push bytes to the coroutine storage. Use to send values between yield and resume.
proc mco_push(co: ptr mco_coro, src: pointer, len: uint): mco_result
## Pop bytes from the coroutine storage. Use to get values between yield and resume.
proc mco_pop(co: ptr mco_coro, dest: pointer, len: uint): mco_result
## Like `mco_pop` but it does not consumes the storage.
proc mco_peek(co: ptr mco_coro, dest: pointer, len: uint): mco_result
## Get the available bytes that can be retrieved with a `mco_pop`.
proc mco_get_bytes_stored(co: ptr mco_coro): uint
## Get the total storage size.
proc mco_get_storage_size(co: ptr mco_coro): uint

# Misc functions.

## Returns the running coroutine for the current thread.
proc mco_running(): ptr mco_coro
## Get the description of a result.
proc mco_result_description(res: mco_result): cstring
{.pop.}


type
  CoroError* = object of CatchableError
    code*: CoroResult

proc `$`*(res: CoroResult): string =
  mco_result_description(res).`$`

proc raise_result(res: CoroResult) =
  if res != CoroResult.SUCCESS:
    raise (ref CoroError)(msg: $res, code: res)

proc initCoroDesc*(co: CoroEntry, stack_size: uint): CoroDesc =
  mco_desc_init(co, stack_size)

using co: Coro

proc init*(co; desc: CoroDesc) = raise_result mco_init(co, desc.addr)
proc uninit*(co) = raise_result mco_uninit(co)

proc create*(desc: CoroDesc): Coro =
  ## Remember to call `co.destroy()` to destroy the coroutine
  var co: Coro
  raise_result mco_create(co.addr, desc.addr)
  co

proc destroy*(co) = raise_result mco_destroy(co)

proc resume*(co) =
  # jump in
  let frame = getFrameState()
  raise_result mco_resume(co)
  setFrameState(frame)

proc `yield`*(co) =
  # jump out
  let frame = getFrameState()
  raise_result mco_yield(co)
  setFrameState(frame)

proc status*(co): CoroState = mco_status(co)

proc userData*(co): pointer = mco_get_user_data(co)
proc pushBytes*(co; data: openArray[uint8]) = raise_result mco_push(co, data[0].addr, data.len.uint)
proc pushRaw*(co; p: pointer; len: int) = raise_result mco_push(co, p, len.uint)
proc push*[T](co; data: T) = raise_result mco_push(co, data.addr, T.sizeof.uint)
proc popBytes*(co; data: var openArray[uint8]) = raise_result mco_pop(co, data[0].addr, data.len.uint)
proc popRaw*(co; p: pointer; len: int) = raise_result mco_pop(co, p, len.uint)
proc pop*[T](co; data: var T) = raise_result mco_pop(co, data.addr, T.sizeof.uint)
proc popDiscard*(co; len: int) = raise_result mco_pop(co, nil, len.uint)
proc peekBytes*(co; data: var openArray[uint8]) = raise_result mco_peek(co, data[0].addr, data.len.uint)
proc peekRaw*(co; p: pointer; len: int) = raise_result mco_peek(co, p, len.uint)
proc peek*[T](co; data: var T) = raise_result mco_peek(co, data.addr, T.sizeof.uint)
proc bytesStored*(co): int = mco_get_bytes_stored(co).int
proc storageSize*(co): int = mco_get_storage_size(co).int
proc running*(): Coro = mco_running()
